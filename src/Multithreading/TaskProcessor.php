<?php


namespace Logema\Utils\Multithreading;

use Logema\Utils\Multithreading\Interfaces\TaskInterface;
use Logema\Utils\Multithreading\Interfaces\TaskProcessorInterface;

/**
 * Класс конечно пока жиденький, но это пока
 *
 * @package Logema\Utils\Multithreading
 */
class TaskProcessor implements TaskProcessorInterface
{
	/**
	 * @param TaskInterface $task
	 * @return \Bitrix\Main\Result
	 */
	public function process(TaskInterface $task)
	{
		return $task->run();
	}
}