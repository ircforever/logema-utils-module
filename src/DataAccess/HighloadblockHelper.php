<?php


namespace Logema\Utils\DataAccess;

use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;
use Bitrix\Highloadblock as HL;

class HighloadblockHelper
{
	protected $hlId = -1;

	/** @var \Bitrix\Highloadblock\DataManager */
	protected $entityClass = null;

	protected $defaultSelectFields = ['*'];

	/**
	 * @param int $hlId
	 */
	protected function __construct(int $hlId)
	{
		$this->hlId = $hlId;

		$this->compileEntity();
	}

	/**
	 * @param array $fields
	 * @return HighloadblockHelper
	 */
	public function setDefaultSelectFields(array $fields)
	{
		$this->defaultSelectFields = $fields;

		return $this;
	}

	/**
	 * @param int $hlId
	 * @return static
	 */
	public static function forHighloadblock(int $hlId)
	{
		static $instances = [];
		if (!array_key_exists($hlId, $instances)) {
			$instances[$hlId] = new static($hlId);
		}

		return $instances[$hlId];
	}

	public function getElementById($id, $select = [])
	{
		return static::getElementByFilter([
			'=ID' => $id
		], $select);
	}

	public function getElementByXmlId($xmlId, $select = [])
	{
		return static::getElementByFilter([
			'=UF_XML_ID' => $xmlId
		], $select);
	}

	public function getElementByFilter($filter, $select = [], $sort = [])
	{
		$listOfOne = $this->getElementsByFilter($filter, $select, 1, $sort);

		return current($listOfOne);
	}

	public function getElementsByFilter($filter = [], $select = [], $limit = 0, $sort = [])
	{
		$selectToSet = $select ?: $this->defaultSelectFields;
		if (array_search('ID', $selectToSet) === false) {
			$selectToSet[] = 'ID';
		}

		$q = $this->getQuery()
			->setSelect($selectToSet)
			->setFilter($filter);

		if ($sort) {
			$q->setOrder($sort);
		}

		if ($limit) {
			$q->setLimit($limit);
		}

		$result = $q->exec();

		$all = [];
		while ($row = $result->Fetch()) {
			$all[(int)$row['ID']] = $row;
		}

		return $all;
	}

	public function getQuery() : \Bitrix\Main\Entity\Query
	{
		return $this->entityClass::query();
	}

	/**
	 * @return \Bitrix\Highloadblock\DataManager
	 */
	public function getEntityClass()
	{
		return $this->entityClass;
	}

	protected function compileEntity()
	{
		Loader::includeModule('highloadblock');

		$hlblock = HL\HighloadBlockTable::getById($this->hlId)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);

		if ($entity) {
			$this->entityClass = $entity->getDataClass();
		} else {
			throw new SystemException("Hl with {$this->hlId} not found");
		}
	}
}
