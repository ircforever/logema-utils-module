<?php


namespace Logema\Utils\Multithreading;

use Bitrix\Main\NotImplementedException;

/**
 * Обертка надо процессом ОС
 * Вдохновлялся оным в Symfony
 *
 * @package Logema\Utils\Multithreading
 */
class Process
{
	const TYPE_STDOUT = 0;
	const TYPE_STDERR = 1;

	/** @var resource */
	protected $processStream = null;
	/** @var resource */
	protected $inputStream = null;
	/** @var resource */
	protected $outputStream = null;
	/** @var resource */
	protected $errorStream = null;

	/** @var string */
	protected $input = '';

	protected $success = false;
	protected $command = '';

	/**
	 * @param string $command
	 */
	public function __construct(string $command)
	{
		$this->command = $command;
	}

	public function getInput()
	{
		return $this->input;
	}

	public function setInput(string $input)
	{
		$this->input = $input;
	}

	public function getInputStream()
	{
		return $this->inputStream;
	}

	public function getOutputStream()
	{
		return $this->outputStream;
	}

	public function getProcessStream()
	{
		return $this->processStream;
	}

	public function getErrorStream()
	{
		return $this->errorStream;
	}

	public function isRunning()
	{
		$status = proc_get_status($this->processStream);

		return $status['running'] == true;
	}

	public function isSuccess()
	{
		return $this->success;
	}

	public function start()
	{
		$command = $this->command;

		[
			$this->processStream,
			$this->inputStream,
			$this->outputStream,
			$this->errorStream,
		] = $this->startProcess($command);

		//Метод заблокирован, пока не запишем весь ввод
		while ($this->isRunning() && mb_strlen($this->input)) {
			$read = [];

			$write = [];
			$write[] = $this->inputStream;

			$except = [];

			$readyStreams = stream_select($read, $write, $except, 5);

			if ($readyStreams === false) {
				$this->success = false;
			} elseif ($readyStreams > 0) {
				if (mb_strlen($this->input)) {
					$written = fputs($this->inputStream, $this->input);

					if ($written === false) {
						$this->success = false;
					} elseif ($written > 0) {
						$this->input = substr($this->input, $written);
						if (mb_strlen($this->input) == 0) {
							fclose($this->inputStream);
						}
					}
				}
			}
		}
	}

	/**
	 * @param callable $callback ($type, $data)
	 * @throws NotImplementedException
	 */
	public function wait($callback = null)
	{
		throw new NotImplementedException();
	}

	protected function startProcess($command)
	{
		$pipes = [];
		$resource = proc_open($command, [
			0 => ['pipe', 'r'], //stdin процесса, мы туда пишем
			1 => ['pipe', 'w'], //stdout процесса, мы оттуда читаем
			2 => ['pipe', 'w'], //stderr процесса, мы оттуда читаем
		],
			$pipes);

		return [$resource, $pipes[0], $pipes[1], $pipes[2]];
	}

	public function isTerminated()
	{
		$status = proc_get_status($this->processStream);

		return $status['running'] !== true;
	}


}