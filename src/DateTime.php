<?php


namespace Logema\Utils;

use \Bitrix\Main\Type\DateTime as BxDateTime;
use \Bitrix\Main\Type\Date as BxDate;
use \DateTime as PhpDateTime;

class DateTime
{
	/**
	 * @var int
	 */
	protected $timestamp;

	/**
	 * DateTime constructor.
	 * @param $timestamp
	 */
	protected function __construct($timestamp)
	{
		$this->timestamp = $timestamp;
	}

	/**
	 * @return DateTime
	 */
	public function clone(): DateTime
	{
		return new static($this->timestamp);
	}

	/**
	 * @param string $format
	 * @return string
	 */
	public function oldBitrixFormat(string $format): string
	{
		return FormatDate($format, $this->timestamp);
	}

	/**
	 * @param string $format
	 * @return string
	 */
	public function bitrixFormat(string $format): string
	{
		return $this->toBitrixDateTime()->format($format);
	}

	/**
	 * @param string $format
	 * @return string
	 */
	public function phpFormat(string $format): string
	{
		return $this->toPhpDateTime()->format($format);
	}

	/**
	 * @param string $format
	 * @return string
	 */
	public function format(string $format = 'd.m.Y H:i:s'): string
	{
		return $this->bitrixFormat($format);
	}

	/**
	 * @return BxDate
	 */
	public function toPhpDateTime(): PhpDateTime
	{
		return new PhpDateTime('@' . $this->timestamp);
	}

	/**
	 * @return BxDate
	 */
	public function toBitrixDate(): BxDate
	{
		return BxDate::createFromTimestamp($this->timestamp);
	}

	/**
	 * @return BxDateTime
	 */
	public function toBitrixDateTime(): BxDateTime
	{
		return BxDateTime::createFromTimestamp($this->timestamp);
	}

	/**
	 * @return DateTime
	 */
	public static function now(): DateTime
	{
		return new static(time());
	}

	/**
	 * @param int $timestamp
	 * @return DateTime
	 */
	public static function fromTimestamp(int $timestamp): DateTime
	{
		return new static($timestamp);
	}

	/**
	 * @param BxDateTime $bxDateTime
	 * @return DateTime
	 */
	public static function fromBitrixDateTime(BxDateTime $bxDateTime): DateTime
	{
		return new static($bxDateTime->getTimestamp());
	}

	/**
	 * @param BxDate $bxDate
	 * @return DateTime
	 */
	public static function fromBitrixDate(BxDate $bxDate): DateTime
	{
		return new static($bxDate->getTimestamp());
	}

	/**
	 * @param string $dateStr
	 * @return DateTime
	 */
	public static function fromString(string $dateStr): DateTime
	{
		return static::fromBitrixDateTime(new BxDateTime($dateStr));
	}

	/**
	 * @param PhpDateTime $phpDateTime
	 * @return DateTime
	 */
	public static function fromPhpDateTime(PhpDateTime $phpDateTime): DateTime
	{
		return new static($phpDateTime->getTimestamp());
	}

	/**
	 * @return int
	 */
	public function getTimestamp(): int
	{
		return $this->timestamp;
	}

	/**
	 * @param int $timestamp
	 */
	public function setTimestamp(int $timestamp): void
	{
		$this->timestamp = $timestamp;
	}
}
