<?php


namespace Logema\Utils\DataAccess;


use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\SectionTable;
use CIBlockElement;
use CIBlockSection;
use CUser;

class IblockHelper
{
	protected $iblockId = -1;

	protected static $iblockPropertiesCache = null;
	protected static $iblockPropertiesEnumCache = null;

	protected $defaultSelectFields = [
		"ID",
		"XML_ID",
		"IBLOCK_ID",
		"IBLOCK_SECTION_ID",
		"NAME",
		"ACTIVE_FROM",
		"ACTIVE_TO",
		"TIMESTAMP_X",
		"DETAIL_PAGE_URL",
		"LIST_PAGE_URL",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"PREVIEW_PICTURE"
	];

	/**
	 * @param int $iblockId
	 */
	protected function __construct(int $iblockId)
	{
		$this->iblockId = $iblockId;
	}

	/**
	 * @param int $iblockId
	 * @return static
	 */
	public static function forIblock(int $iblockId)
	{
		static $instances = [];
		if (!array_key_exists($iblockId, $instances)) {
			$instances[$iblockId] = new static($iblockId);
		}

		return $instances[$iblockId];
	}

	/**
	 * @param array $fields
	 * @return IblockHelper
	 */
	public function setDefaultSelectFields(array $fields)
	{
		$this->defaultSelectFields = $fields;

		return $this;
	}

	/**
	 * Принадлежность секции к инфоблоку
	 *
	 * @param int $sectionId
	 * @return bool
	 */
	public function isOwnSection(int $sectionId)
	{
		$element = SectionTable::query()
			->setSelect(['ID'])
			->setFilter(['=ID' => $sectionId, '=IBLOCK_ID' => $this->iblockId])
			->exec()
			->fetch();

		return $element !== false;
	}

	/**
	 * Принадлежность элемента к инфоблоку
	 *
	 * @param int $elementId
	 * @return bool
	 */
	public function isOwnElement(int $elementId)
	{
		$element = ElementTable::query()
			->setSelect(['ID'])
			->setSelect(['ID'])
			->setFilter(['=ID' => $elementId, '=IBLOCK_ID' => $this->iblockId])
			->exec()
			->fetch();

		return $element !== false;
	}

	public function getElementById(int $id, $select = [])
	{
		return static::getElementByFilter([
			'ID' => $id
		], $select);
	}

	public function getElementsByIds($ids, $select = [], $sort = [], $additionalFilter = [])
	{
		$filter = array_merge($additionalFilter, [
			'ID' => $ids
		]);

		return static::getElementsByFilter($filter, $select, $sort);
	}

	public function getElementByXmlId(string $xmlId, $select = [])
	{
		return static::getElementByFilter([
			'XML_ID' => $xmlId
		], $select);
	}

	public function getElementByFilter($filter = [], $select = [], $sort = [])
	{
		$filter = array_merge($filter, [
			'IBLOCK_ID' => $this->iblockId,
		]);

		$listOfOne = $this->getElementsByFilter($filter, $select, 1, $sort);

		return current($listOfOne);
	}

	public function getElementsByFilter($filter = [], $select = [], $limit = 0, $sort = [])
	{
		$navParams = false;
		if ($limit) {
			$navParams = ['nTopCount' => $limit];
		}

		return $this->getElementsByFilterAndNavParams($filter, $select, $navParams, $sort);
	}

	protected function getElementsByFilterAndNavParams($filter, $select, $navPrams, $sort)
	{
		$filter = array_merge($filter, [
			'IBLOCK_ID' => $this->iblockId,
		]);

		if ($select) {
			$select = array_merge($select, ['ID', 'IBLOCK_ID']);
		} else {
			$select = array_merge($this->defaultSelectFields, ['ID', 'IBLOCK_ID']);
		}

		$propertyCodes = [];
		foreach ($select as $item) {
			$matches = [];
			if (preg_match('/^PROPERTY_(.*)$/i', $item, $matches)) {
				$propertyCodes[] = $matches[1];
			}
		}

		if (count($propertyCodes) > 0) {
			return $this->retrieveElementsWithProperties($filter, $select, $navPrams, $sort, $propertyCodes);
		} else {
			return $this->retrieveElementsWithoutProperties($filter, $select, $navPrams, $sort);
		}
	}

	public function getElementsForPage($filter = [], $count = 10, $pageNumber = 1, $select = [], $sort = [])
	{
		$navPrams = [
			'iNumPage' => $pageNumber,
			'nPageSize' => $count
		];

		return $this->getElementsByFilterAndNavParams($filter, $select, $navPrams, $sort);
	}

	public function getCount($filter, $group = [])
	{
		$filter['IBLOCK_ID'] = $this->iblockId;
		return (int)\CIBlockElement::GetList([], $filter, $group);
	}

	protected function retrieveElementsWithProperties($filter, $select, $nav, $sort, $propertyCodes)
	{
		$elements = [];
		$iterator = \CIBlockElement::GetList($sort, $filter, false, $nav, $select);
		while ($row = $iterator->GetNext()) {
			$row['PROPERTIES'] = [];
			$elements[(int)$row['ID']] =& $row;
			unset($row);
		}

		//Если запросили PROPERTY_*, то просто не фильтруем свойства
		//TODO: Научить понимать свойтсва по ID, если это когда-нибудь понадобится
		$propertyFilter = [];
		if (array_search('*', $propertyCodes) === false) {
			$propertyFilter['CODE'] = $propertyCodes;
		}

		\CIBlockElement::GetPropertyValuesArray($elements, $filter['IBLOCK_ID'], $filter, $propertyFilter);
		unset($rows, $filter, $order);

		return $elements;
	}

	protected function retrieveElementsWithoutProperties($filter, $select, $nav, $sort)
	{
		$elements = [];
		$iterator = \CIBlockElement::GetList($sort, $filter, false, $nav, $select);
		while ($row = $iterator->GetNext()) {
			$elements[(int)$row['ID']] = $row;
		}

		return $elements;
	}

	/**
	 * Значение свойтва в событии добавления/обновления
	 * Со всеми особенностями админки и прямых запросов
	 *
	 * TODO: Поддержка множественных значений свойств
	 *
	 * @param array $fields
	 * @param mixed $property
	 *
	 * @return mixed null если нет значения, '' если пустое, строка если есть
	 */
	public static function getChangeEventPropertyValue($fields, $property)
	{
		$propertyValues = &$fields['PROPERTY_VALUES'];

		$code = $property['CODE'];
		if (isset($propertyValues[$code])) {
			if (is_array($propertyValues[$code])) {
				$value = current($propertyValues[$code]);
				return (is_array($value)) ? $value['VALUE'] : $value;
			}

			return $propertyValues[$code];
		}

		$code = $property['ID'];
		if (isset($propertyValues[$code])) {
			if (is_array($propertyValues[$code])) {
				$value = current($propertyValues[$code]);
				return (is_array($value)) ? $value['VALUE'] : $value;
			}

			return $propertyValues[$code];
		}

		return null;
	}

	/**
	 * @return IblockPropertyHelper
	 */
	public function getIblockPropertyHelper()
	{
		return IblockPropertyHelper::forIblock($this->iblockId);
	}

	/**
	 * @param string $code
	 * @param array $select
	 * @return array
	 */
	public function getElementByCode($code, $select = [])
	{
		return $this->getElementByFilter(['CODE' => $code], $select);
	}

	/**
	 * @param int[] $ids
	 * @param array $select
	 * @return array
	 */
	public function getSectionsByIds($ids, $select = [])
	{
		return $this->getSectionsByFilter(['ID' => $ids], $select);
	}

	/**
	 * @param string $code
	 * @param array $select
	 * @return array
	 */
	public function getSectionByCode($code, $select = [])
	{
		return $this->getSectionsByFilter(['CODE' => $code], $select);
	}

	/**
	 * @param int $id
	 * @param array $select
	 * @return array
	 */
	public function getSectionById($id, $select = [])
	{
		return current($this->getSectionsByIds([$id], $select));
	}

	/**
	 * @param array $filter
	 * @param array $select
	 * @param array $sort
	 * @return array
	 */
	public function getSectionByFilter($filter = [], $select = [], $sort = [])
	{
		return current($this->getSectionsByFilter($filter, $select, 1, $sort));
	}

	/**
	 * @param array $filter
	 * @param array $select
	 * @param int $limit
	 * @param array $sort
	 * @return array
	 */
	public function getSectionsByFilter($filter = [], $select = [], $limit = 0, $sort = [])
	{
		// Постраничка
		$navParams = false;
		if ($limit) {
			$navParams = ['nTopCount' => $limit];
		}

		// Подставим инфоблок текущей сущности
		$filter['IBLOCK_ID'] = $this->iblockId;

		// Проверим не забыли ли указать ID
		if (!in_array('ID', $select)) {
			$select[] = 'ID';
		}

		$iterator = CIBlockSection::GetList($sort, $filter, false, $select, $navParams);
		$sections = [];
		while ($row = $iterator->Fetch()) {
			$sections[$row['ID']] = $row;
		}

		return $sections;
	}

	/**
	 * @param array $fields Массив полей без свойств
	 * @param array $props Массив свойств
	 * @return int ID добавленного элемента, 0 при ошибке
	 */
	public function addElement($fields = [], $props = [])
	{
		global $USER;
		if (!$fields['MODIFIED_BY'] && $USER instanceof CUser && !is_null($USER) && $USER->GetID()) {
			$fields['MODIFIED_BY'] = $USER->GetID();
		}

		if (!$fields['IBLOCK_ID']) {
			$fields['IBLOCK_ID'] = $this->iblockId;
		}

		if (is_array($props) && !empty($props)) {
			$fields['PROPERTY_VALUES'] = $props;
		}

		$element = new CIBlockElement;
		return (int)$element->Add($fields);
	}

	public function deleteElement(int $id)
	{
		$element = new CIBlockElement;
		CIBlockElement::Delete($id);
	}

	/**
	 * @param int $id
	 * @return bool
	 */
	public function activateElement(int $id): bool
	{
		return (new CIBlockElement)->Update($id, ['ACTIVE' => 'Y']);
	}

	/**
	 * @param int $id
	 * @return bool
	 */
	public function deactivateElement($id): bool
	{
		return (new CIBlockElement)->Update($id, ['ACTIVE' => 'N']);
	}
}
