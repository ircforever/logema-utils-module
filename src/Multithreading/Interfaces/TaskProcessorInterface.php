<?php


namespace Logema\Utils\Multithreading\Interfaces;


interface TaskProcessorInterface
{
	/**
	 * @param TaskInterface $task
	 * @return \Bitrix\Main\Result
	 */
	public function process(TaskInterface $task);
}