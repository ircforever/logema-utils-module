<?php


namespace Logema\Utils\Multithreading;

class WorkerProcessResult extends \Bitrix\Main\Result
{
	public function setStdout($stdout)
	{
		$this->data['STDOUT'] = $stdout;
	}

	public function getStdout()
	{
		return $this->data['STDOUT'];
	}

	public function setChunkProcessResult(ChunkProcessResult $result)
	{
		$this->data['CHUNK_RESULT'] = $result;
	}

	public function getChunkProcessResult(): ?ChunkProcessResult
	{
		return $this->data['CHUNK_RESULT'];
	}
}