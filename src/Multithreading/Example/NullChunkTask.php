<?php

namespace Logema\Utils\Multithreading\Example;


use Bitrix\Iblock\ElementTable;
use Bitrix\Main\DB\ResultIterator;
use Bitrix\Main\Error;
use Bitrix\Main\Result;
use Logema\Utils\Multithreading\ChunkProcessResult;
use Logema\Utils\Multithreading\Interfaces\ChunkableTaskInterface;

/**
 * Пример, поясняющий работу с ChunkTask
 *
 * В данном случае - изменяет активность элементов инфоблока в зависимости от четности сегодняшнего числа
 * Если день четный - пусть четные элементы будут активны, а нечентный - нечетные.
 * Количество измененных элементов надо отправить на email, а если их окажется 100 (сто) - это трагедия и надо считать,
 * что скрипт завершен неудачно
 *
 * @see ChunkableTaskInterface
 * @see nullchunktask_cron.php
 * @example
 *
 * @package Logema\Utils\Multithreading
 */
class NullChunkTask implements ChunkableTaskInterface
{
	protected $changedRows = 0;
	protected $params = [];

	//region=Работа в несколько потоков

	/**
	 * Возвращает итератор по списку элементов к обработке.
	 *
	 * @note Вернем интератор по всем элементам нашего инфоблока
	 * @return \Iterator
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public function getRowIterator()
	{
		return new ResultIterator(ElementTable::getList([
			'select' => ['ID', 'ACTIVE', 'IBLOCK_ID'],
			'filter' => [
				'=IBLOCK_ID' => -1,
			],
			'order' => ['ID' => 'asc']
		]));
	}

	/**
	 * Обрабатывает набор строк [в отдельном процессе]
	 *
	 * @note В этот метод попадут getChunkSize() строк итератора из getRowIterator()
	 * @exmaple [
	 *  ['ID' => 7, 'ACTIVE' => 'Y', 'IBLOCK_ID' => -1],
	 *  ...
	 * ]
	 *
	 * @param array $rows
	 * @return ChunkProcessResult
	 */
	public static function processChunk($rows): ChunkProcessResult
	{
		//Выполним полезную работу
		$changed = static::process($rows);

		//Передадим результат работы в основной поток
		$result = new ChunkProcessResult();
		$result->setData(['CHANGED' => $changed]);

		return $result;
	}

	/**
	 * Желаемый размер чанка в строках
	 *
	 * @note подбирается эксперименталь так, чтобы никогда не падать по памяти
	 *
	 * @return int
	 */
	public function getChunkSize()
	{
		return 500;
	}

	/**
	 * Сохраняет нужную для finalize() информацию из результата processChunk
	 *
	 * @note результат из processChunk() будет передан в основной поток
	 *
	 * @param ChunkProcessResult $result
	 * @return mixed
	 */
	public function saveChunkData(ChunkProcessResult $result)
	{
		$changed = $result->getData()['CHANGED'];

		$this->changedRows += $changed;
	}

	/**
	 * Выполняет завершающие действия
	 * @return Result
	 * @example набор обработанных ID, для того чтобы все прочие деактивировать.
	 */
	public function finalize(): Result
	{
		$result = new Result();
		$errorRowsCount = $this->getParams()['errorRowsCount'];

		if ($this->changedRows == 100) {
			$result->addError(new Error("Изменено {$errorRowsCount} строк"));
		}

		return $result;
	}
	//endregion

	//region=Полезная работа
	/**
	 * @param $rows
	 * @return int количество измененных строк
	 */
	protected static function process($rows)
	{
		//Обычно на основании ID из строк загружаются элементы с ценами, свойствами и пр.
		//$rows = static:loadData(array_filter(function($element) {return (int) $element['ID'];}, $rows));
		//Но в данном случае вся нужная информация уже есть

		$now = date('j');
		$nowRemainder = ($now % 2 === 0)
			? 1
			: 0;

		$changed = 0;
		foreach ($rows as $row) {

			$rowActivity = ($rows['ID'] % 2 == $nowRemainder)
				? 'Y'
				: 'N';

			if ($rows['ACTIVE'] != $rowActivity) {

				$el = new \CIBlockElement();
				$updated = $el->Update($row['ID'], ['ACTIVE' => $rowActivity]);
				if ($updated) {
					$changed++;
				}
			}
		}

		return $changed;
	}

	//endregion

	public function initialize()
	{
		$this->setParams(array_merge($this->getParams(), ['errorRowsCount' => 100]));
	}

	public function setParams(array $params): void
	{
		$this->params = $params;
	}

	public function getParams(): array
	{
		return $this->params;
	}
}