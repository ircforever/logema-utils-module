<?php


namespace Logema\Utils\Multithreading\Interfaces;


interface TaskInterface
{
	/**
	 * @return \Bitrix\Main\Result
	 */
	public function run();

	public function setParams(array $params): void;

	public function getParams(): array;
}