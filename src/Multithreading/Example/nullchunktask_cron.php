<?php

use Logema\Utils\Multithreading\Example\NullChunkTask;
use Logema\Utils\Multithreading\PoolChunkableTaskProcessor;

define("NO_KEEP_STATISTIC", true);
define('CHK_EVENT', false);
define('NO_AGENT_CHECK', true);
define("STATISTIC_SKIP_ACTIVITY_CHECK", true);
define('BX_SECURITY_SESSION_VIRTUAL', true);
define('SITE_ID', 's1');

$_SERVER['DOCUMENT_ROOT'] = dirname(__DIR__, 3);
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

while (ob_get_level()) {
	ob_end_clean();
}

error_reporting(0);

//Если нужно защититься от параллельного запуска нескольких копий
if (count($argv) == 1) {
	$me = fopen(__FILE__, 'r');
	$locked = flock($me, LOCK_EX | LOCK_NB);
	if (!$locked) {
		die();
	}
}

$task = new NullChunkTask();
$task->setParams(['testParam' => 'test']);//Пример задания параметров задачи
$processor = new PoolChunkableTaskProcessor(4, $argv); //Мы будем обрабатывать задачу в 4 потока
$result = $processor->process($task);

if (!$result->isSuccess()) {
	echo "Ошибка:" . implode("\n", $result->getErrorMessages());

	exit(1);
}