<?php


namespace Logema\Utils\Multithreading;

use Bitrix\Main\Error;
use Bitrix\Main\Result;
use Logema\Utils\Multithreading\Interfaces\ChunkableTaskInterface;
use Logema\Utils\Multithreading\Interfaces\ChunkableTaskProcessorInterface;

/**
 * Запускает ChunkTaskInterface на пуле процессов ChunkTaskWorker
 */
class PoolChunkableTaskProcessor implements ChunkableTaskProcessorInterface
{
	/** @var int */
	protected $concurrency = 1;
	/** @var WorkerPool */
	protected $pool = null;
	/** @var array */
	protected $accumulator = [];

	/** @var ChunkableTaskInterface */
	protected $task = null;

	/** @var string[] */
	protected $errors;
	/** @var bool */
	protected $stop = false;

	/** @var Result */
	protected $result;
	/** @var array */
	private $argv;

	/**
	 * @param int $concurrency
	 * @param array $argv
	 */
	public function __construct($concurrency, $argv)
	{
		$this->concurrency = $concurrency;
		$this->argv = $argv;
	}

	public function process(ChunkableTaskInterface $task)
	{
		$this->task = $task;

		//Что же нам запустить, мастер или воркер?
		if ($this->argv[1] !== 'worker') {
			return $this->runMaster();
		} else {
			$this->runSlave();
		}
	}

	protected static function lock()
	{
		static::$me = fopen(__FILE__, 'r');
		return flock(static::$me, LOCK_EX | LOCK_NB);
	}

	protected static function unlock()
	{
		return flock(static::$me, LOCK_UN);
	}

	protected function runSingleThreadWorker()
	{
		$chunkResult = $this->task::processChunk($this->accumulator);

		if ($chunkResult->isSuccess()) {
			$this->task->saveChunkData($chunkResult);
		} else {
			$this->result->addErrors($chunkResult->getErrors());
			$this->stopBecause("Ошибка импорта чанка");
		}

		$this->accumulator = [];
	}

	protected function runMultiThreadWorker()
	{
		$importer = &$this;

		$this->pool->add(new ChunkTaskWorker(
			[
				"TASK" => $this->task,
				"ACCUMULATOR" => $this->accumulator,
			],
			function (WorkerProcessResult $result) use ($importer) {
				$importer->processThreadResult($result);
			}
		));

		$this->accumulator = [];
	}

	protected function processThreadResult(WorkerProcessResult $result)
	{
		echo $result->getStdout();

		if ($result->isSuccess()) {
			$chunkResult = $result->getChunkProcessResult();
			$this->task->saveChunkData($chunkResult);
		} else {
//            $this->logger->error('Один из потоков завершился ошибкой, экстренная остановка');
			$this->stopBecause('Ошибка в потоке');
			$this->result->addErrors($result->getErrors());
		}
	}

	public function runMaster()
	{
		$this->result = new Result();
		$this->pool = new WorkerPool($this->concurrency);
		$this->stop = false;

		$this->task->initialize();

		$iterator = $this->task->getRowIterator();
		foreach ($iterator as $row) {
			if ($this->stop) {
				break;
			}

			$this->accumulator[] = $row;
			if (count($this->accumulator) >= $this->task->getChunkSize()) {
				$this->runAccumulator();
			}
		}

		if (count($this->accumulator) && !$this->stop) {
			$this->runAccumulator();
		}

		$this->pool->wait();

		$this->finalizeTask();

		return $this->result;
	}

	protected function runSlave()
	{
		ChunkTaskWorker::runRunFromInput();
	}

	protected function stopBecause($reason)
	{
		$this->result->addError(new Error($reason));
		$this->stop = true;
	}

	protected function runAccumulator()
	{
		if ($this->concurrency <= 1) {
			$this->runSingleThreadWorker();
		} else {
			$this->runMultiThreadWorker();
		}
	}

	protected function finalizeTask()
	{
		$result = $this->task->finalize();

		if (!$result->isSuccess()) {
			$this->result->addErrors($result->getErrors());
		}
	}
}