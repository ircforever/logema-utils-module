<?php

namespace Logema\Utils\Iterators;

use Bitrix\Main\NotSupportedException;
use Bitrix\Sale\Compatible\CDBResult;

/**
 * Class CDbResultIterator
 * @package Bitrix\Main\DB
 */
class CDbResultIterator implements \Iterator
{
	const FETCH_TYPE_FETCH = 0;
	const FETCH_TYPE_GET_NEXT = 1;

	/** @var CDBResult */
	private $result;
	/** @var int */
	private $counter;
	private $currentData;
	/** @var int */
	private $fetchType;

	/**
	 * ResultIterator constructor.
	 *
	 * @param CDBResult $result
	 * @param int $fetchType
	 */
	public function __construct(CDBResult $result, $fetchType = self::FETCH_TYPE_FETCH)
	{
		$this->result = $result;
		$this->counter = -1;
		$this->fetchType = $fetchType;
	}

	/**
	 * Return the current element
	 * @link http://php.net/manual/en/iterator.current.php
	 * @return mixed Can return any type.
	 * @since 5.0.0
	 */
	public function current()
	{
		return $this->currentData;
	}

	/**
	 * Move forward to next element
	 * @link http://php.net/manual/en/iterator.next.php
	 * @return void Any returned value is ignored.
	 * @since 5.0.0
	 */
	public function next()
	{
		if ($this->fetchType == self::FETCH_TYPE_FETCH) {
			$this->currentData = $this->result->Fetch();
		} else {
			$this->currentData = $this->result->GetNext();
		}
		$this->counter++;
	}

	/**
	 * Return the key of the current element
	 * @link http://php.net/manual/en/iterator.key.php
	 * @return mixed scalar on success, or null on failure.
	 * @since 5.0.0
	 */
	public function key()
	{
		return $this->counter;
	}

	/**
	 * Checks if current position is valid
	 * @link http://php.net/manual/en/iterator.valid.php
	 * @return boolean The return value will be casted to boolean and then evaluated.
	 * Returns true on success or false on failure.
	 * @since 5.0.0
	 */
	public function valid()
	{
		return $this->currentData !== false;
	}

	/**
	 * Rewind the Iterator to the first element
	 * @link http://php.net/manual/en/iterator.rewind.php
	 * @throws NotSupportedException
	 * @since 5.0.0
	 */
	public function rewind()
	{
		if ($this->counter > 0) {
			throw new NotSupportedException('Could not rewind the iterator');
		}

		$this->next();
	}
}