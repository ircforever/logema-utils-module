<?php


namespace Logema\Utils\Multithreading\Interfaces;


use Bitrix\Main\Result;
use Logema\Utils\Multithreading\ChunkProcessResult;
use Logema\Utils\Multithreading\Example\NullChunkTask;

/**
 * Основной поток                            |                     Отдельный процесс
 * +---------------------------------------+           |
 * |           initialize();               |           |
 * +---------------------------------------+           |
 * |                                  |
 * +----------------V----------------------+           |
 * |                                       |           |
 * |   Из итератора getRowIterator()       |           |
 * |   берутся строки и собираются         |           |
 * |   в пачки по getChunkSize() штук.     |           |
 * |                                       |           |
 * +----------------+----------------------+           |
 * |                                  |
 * |                                  |             +-------------------------------------+
 * |                                  |             |                                     |
 * |                                  |           +-+-----------------------------------+ |
 * |                                  |           |                                     | |
 * +----------------v--------------------------+       |         +-+-----------------------------------+ | |
 * |                                           |       |         |                                     | | |
 * | Каждая пачка отправляется в отдельный     |       |         | processChunk() обрабатывает данные. | | |
 * | процесс php и в нём запускается метод     |                 | Как правило получая-дополнительные  | +-+
 * | processChunk($rows)                       |  mixed[] $rows  | данные из БД и сохраняя что-то туда.| |
 * |                                           +-----------------> Например, записывает свойства в ИБ  +-+
 * | Сразу несколько параллельно.              |                 |                                     |
 * |                                           |       |         +--------------------+----------------+
 * +-------------------------------------------+       |                              | | |
 * |                              | | |
 * |                              | | |
 * +-------------------------------------------+       |                              | | |
 * |                                           |       |                              | | |
 * | saveChunkData(ChunkProcessResult $result) |              ChunkProcessResult      | | |
 * | сохраняет результат каждого процесса      <--------------------------------------+ | |
 * | для финальной обработки                   <----------------------------------------+ |
 * | Например, ID элементов для                <------------------------------------------+
 * | деактивации потом тех элементов           |       |
 * | инфобока, которые не были обработаны      |       |
 * |                                           |       |
 * +-------------------+-----------------------+       |
 * |                               |
 * v                               |
 * +-------------------+-----------------------+       |
 * |                                           |       |
 * | Метод finalize(): Result выполняет        |       |
 * | завершающие операции и возвращает         |       |
 * | результат всего процесса.                 |       |
 * | Например, деактивирует лишнее.            |       |
 * |                                           |       |
 * +-------------------------------------------+       |
 * |
 */

/**
 * Задача, обрабатываемая по кусочкам, в т.ч. в отдельных процессах параллельно
 *
 * @see NullChunkTask
 * @package Logema\Utils\Multithreading
 */
interface ChunkableTaskInterface
{
	/**
	 * Возвращает итератор по списку элементов к обработке
	 *
	 * @return \Iterator
	 */
	public function getRowIterator();

	/**
	 * Обрабатывает набор строк [в отдельном процессе]
	 * Так сказать, map
	 *
	 * @param array $rows
	 * @return ChunkProcessResult
	 */
	public static function processChunk($rows): ChunkProcessResult;

	/**
	 * Желаемый размер чанка в строках
	 *
	 * @return int
	 */
	public function getChunkSize();

	/**
	 * Сохраняет нужную для finalize() информацию из результата processChunk
	 *
	 * @param ChunkProcessResult $result
	 * @return mixed
	 */
	public function saveChunkData(ChunkProcessResult $result);

	/**
	 * Выполняет завершающие действия
	 * Так сказать, reduce
	 *
	 * @return Result
	 * @example набор обработанных ID, для того чтобы все прочие деактивировать.
	 */
	public function finalize(): Result;

	public function initialize();

	public function setParams(array $params): void;

	public function getParams(): array;
}