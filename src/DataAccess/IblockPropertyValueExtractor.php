<?php


namespace Logema\Utils\DataAccess;


use ArrayAccess;
use Bitrix\Main\NotSupportedException;

class IblockPropertyValueExtractor implements ArrayAccess
{
	protected $element = [];

	/**
	 * @param array $element
	 */
	protected function __construct(array &$element)
	{
		$this->element = $element;
	}

	public static function forElement(array &$element)
	{
		if (!$element['PROPERTIES']) {
			throw new \Exception('У элемента нет значений свойств.');
		}

		return new static($element);
	}

	/**
	 * @param array $elements
	 * @return IblockPropertyValueExtractor[]
	 * @throws \Exception
	 */
	public static function forElements(array &$elements)
	{
		$extractors = [];
		foreach ($elements as $kElement => &$element) {
			$extractors[$kElement] = static::forElement($element);
		}

		unset($element);

		return $extractors;
	}

	/**
	 * Возвращает значение свойтства по коду
	 * Элемент передаётся по ссылке для быстродействия
	 *
	 * @param string $code
	 * @param bool $escaped
	 * @return null
	 * @throws \Exception
	 */
	public function getValue($code, $escaped = false)
	{
		if (!array_key_exists($code, $this->element['PROPERTIES'])) {
			return null;
		}

		$property = &$this->element['PROPERTIES'][$code];
		$type = $property['PROPERTY_TYPE'];
		$userType = $property['USER_TYPE'];
		$isMultiply = $property['MULTIPLE'] == 'Y';

		if (!$escaped && !is_set($property['~VALUE'])) {
			throw new \Exception('Нет неэкранированного значения свойства.');
		}

		$value = $escaped ? $property['VALUE'] : $property['~VALUE'];

		if ($type == 'S') { // Строка
			if (in_array($userType, ['Date', 'DateTime'])) {
				if ($isMultiply) {
					$dates = [];
					foreach ($value as $val) {
						$dates[] = \Logema\Utils\DateTime::fromString($val);
					}
					return $dates;
				} else {
					return \Logema\Utils\DateTime::fromString($value);
				}
			} else if ($userType == 'HTML') {
				if ($isMultiply) {
					$texts = [];
					foreach ($value as $val) {
						$valueType = $val['TYPE'];
						if (in_array($valueType, ['TEXT', 'HTML'])) {
							$texts[] = $val['TEXT'];
						} else {
							$texts[] = '';
						}
					}
					return $texts;
				} else {
					$valueType = $value['TYPE'];
					if (in_array($valueType, ['TEXT', 'HTML'])) {
						return $value['TEXT'];
					} else {
						return '';
					}
				}
			}
		}

		return $value;
	}

	public function offsetExists($offset)
	{
		return isset($this->element['PROPERTIES'][$offset]);
	}

	public function offsetGet($offset)
	{
		return isset($this->element['PROPERTIES'][$offset]) ? $this->getValue($offset) : null;
	}

	public function offsetSet($offset, $value)
	{
		throw new NotSupportedException();
	}

	public function offsetUnset($offset)
	{
		throw new NotSupportedException();
	}
}