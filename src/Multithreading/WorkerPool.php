<?php

namespace Logema\Utils\Multithreading;


use Bitrix\Main\Result;

class WorkerPool
{
	protected $size = 0;
	/** @var WorkerRecord[] */
	protected $workers = [];

	protected $emergencyStop = false;
	protected $error;

	/**
	 * WorkerPool constructor.
	 * @param int $size
	 */
	public function __construct(int $size)
	{
		if ($size <= 0) {
			throw new \InvalidArgumentException();
		}

		$this->size = $size;
	}

	public function add(Worker $worker)
	{
		while (count($this->workers) >= $this->size) {
			$this->waitAndProcessStreamUpdates();
		}

		$this->startWorker($worker);
	}

	/**
	 * Ждать завершения всех потоков
	 */
	public function wait()
	{
		while (count($this->workers)) {
			$this->waitAndProcessStreamUpdates();
		}
	}

	/**
	 * @param Worker $worker
	 */
	protected function startWorker($worker)
	{
		$worker->start();

		$record = new WorkerRecord(
			$worker
		);

		$this->workers[$record->id] = $record;
	}

	protected function cleanWorkers()
	{
		if ($this->emergencyStop) {
			foreach ($this->workers as $index => $record) {
				$this->finishWorker($record);
				unset($this->workers[$index]);
			}
		}

		//Вычеркиваем почему-то умерших
		foreach ($this->workers as $index => $record) {
			if ($record->getWorker()->isTerminated()) {
				$this->emergencyStopBecause("Поток #{$index} неожиданно умер");
				unset($this->workers[$index]);
			}
		}
	}

	protected function waitAndProcessStreamUpdates()
	{
		$read = [];
		foreach ($this->workers as $index => $record) {
			$read[$record->id] = $record->getWorker()->getOutputStream();
		}

		$write = [];
		foreach ($this->workers as $index => $record) {
			if (mb_strlen($record->getWorker()->getInput()) > 0) {
				$write[$record->id] = $record->getWorker()->getInputStream();
			}
		}

		$except = [];

		$readyStreams = stream_select($read, $write, $except, 5);

		if ($readyStreams === false) {
			$this->emergencyStopBecause('stream_select завершился неудачно');
		}

		//Запишем тем, кому есть что писать
		foreach ($write as $recordId => $w) {
			$record = $this->workers[$recordId];
			$worker = $record->getWorker();

			$written = fputs($worker->getInputStream(), $worker->getInput());

			if ($written === false) {
				$this->emergencyStopBecause('Не удалось записать данные в поток');
			} else {
				$worker->setInput(substr($worker->getInput(), $written));
				if (mb_strlen($worker->getInput()) == 0) {
					fclose($worker->getInputStream());
				}
			}
		}

		//Прочтем тех, у кого есть что читать
		foreach ($read as $recordId => $w) {
			$record = &$this->workers[$recordId];
			$worker = $record->getWorker();

			$s = fgets($worker->getOutputStream());

			if (is_string($s)) {
				$record->addReceivedData($s);
			} elseif ($s === false) {
				$this->emergencyStopBecause('Не удалось прочитать данные из потока');
			}

			//Завершим те, у кого читать больше нечего
			if (feof($worker->getOutputStream())) { //TODO: Вообщето, надо бы на смотреть на isSuccess/isTerminated процесса
				$this->finishWorker($record);
				$this->processThreadResult($record);
				unset($this->workers[$recordId]);
			}
		}
	}

	protected function emergencyStopBecause($reason)
	{
		$this->emergencyStop = true;
		$this->error = $reason;
	}

	protected function finishWorker(WorkerRecord $record)
	{
		$worker = $record->getWorker();

		@fclose($worker->getInputStream());
		@fclose($worker->getOutputStream());

		$returnValue = proc_close($worker->getProcessStream());

		if ($returnValue != 0) {
			$this->emergencyStopBecause('При завершении потока возникли проблемы');
		}
	}

	protected function processThreadResult(WorkerRecord $record)
	{
		$result = unserialize($record->getReceivedData());
		if ($result instanceof Result) {
			$record->getWorker()->processResult($result);
		} else {
			$this->emergencyStopBecause('Поток завершился корректно, но вернул ерунду');
		}
	}

	/**
	 * @return mixed
	 */
	public function getError()
	{
		return $this->error;
	}
}
