<?php


namespace Logema\Utils\Multithreading;

use Bitrix\Main\Error;
use Bitrix\Main\Result;

/**
 * Процесс, который обрабатывает некие входные данные и возвращает результат в stdout
 * А запускает он сам себя
 *
 * @package Logema\Utils\Multithreading
 */
class Worker extends Process
{
	/** @var callable|null */
	protected $callback = null;
	protected $params = [];

	/**
	 * @param array $params
	 * @param Callable $callback
	 */
	public function __construct(array $params, $callback)
	{
		$this->params = $params;
		$this->input = serialize($params);
		$this->callback = $callback;

		parent::__construct($this->getShellCommand());
	}

	/** @return array */
	public function getParams(): array
	{
		return $this->params;
	}

	/**
	 * @param mixed $params
	 * @return void
	 */
	public function setParams($params)
	{
		$this->params = $params;
	}

	public function run()
	{
		return new Result();
	}

	public static function runRunFromInput()
	{
		$input = file_get_contents('php://stdin');
		$params = unserialize($input);

		if ($params !== false) {
			$worker = new static($params, null);

			ob_start();
			$result = $worker->run();
			while (ob_get_level()) {
				ob_end_clean();
			}

		} else {
			$result = new WorkerProcessResult();
			$result->addError(new Error('Не получены входные параметры'));
		}

		echo serialize($result);
		die();
	}

	/** @return string */
	public function getShellCommand()
	{
		$scriptFilename = $_SERVER['SCRIPT_FILENAME'];
		$param = $this->getShellParam();
		$cmd = "php -f {$scriptFilename} worker {$param}";

		return $cmd;
	}

	public function processResult(Result $result)
	{
		$callable = $this->callback;
		$callable($result);
	}

	protected function getShellParam()
	{
		return '';
	}
}