<?php


namespace Logema\Utils;


class TextHelper
{
	/**
	 * Получить форму слова для исчисления.
	 *
	 * Например, чтобы получить фразу "1 год", 5 "лет", 66 "штук", 80 "пальто"
	 *
	 * @param int $count число
	 * @param string $singleForm единственная форма слова (1 год)
	 * @param string|bool $someForm множественная форма слова для небольших чисел (x2-x4 - 2 года, 4 года)
	 * @param string|bool $manyForm множественная форма слова для больших чисел (x0, x5-x9 - 5 лет, 9 лет, 10 лет)
	 *
	 * @return string
	 */
	public static function pluralForm($count, $singleForm, $someForm = false, $manyForm = false)
	{
		if ($someForm === false)
		{
			$someForm = $singleForm;
		}
		if ($manyForm === false)
		{
			$manyForm = $someForm;
		}

		$count = abs($count) % 100;
		$count1 = $count % 10;
		if ($count > 10 && $count < 20) return $manyForm;
		if ($count1 > 1 && $count1 < 5) return $someForm;
		if ($count1 == 1) return $singleForm;
		return $manyForm;
	}
}