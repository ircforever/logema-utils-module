<?php


namespace Logema\Utils\Multithreading;


use Bitrix\Main\Error;
use Bitrix\Main\Result;
use Logema\Utils\Multithreading\Interfaces\ChunkableTaskInterface;

class ChunkTaskWorker extends Worker
{
	/** @var ChunkableTaskInterface|null */
	protected $task = null;

	/**
	 * @return WorkerProcessResult
	 */
	public function run()
	{
		$this->task = $this->params['TASK'];

		ob_start();
		$chunkResult = $this->task::processChunk($this->params['ACCUMULATOR']);
		$stdout = ob_get_clean();

		$workerResult = new WorkerProcessResult();
		$workerResult->setChunkProcessResult($chunkResult);
		$workerResult->setStdout($stdout);

		if (!$chunkResult->isSuccess()) {
			$workerResult->addError(new Error("Ошибка в обработке чанка"));
		}

		return $workerResult;
	}

	public function processResult(Result $result)
	{
		$callable = $this->callback;

		if ($result instanceof WorkerProcessResult) {
			$callable($result);
		} else {
			throw new \InvalidArgumentException('Mama mia, $result is not instance of WorkerProcessResult');
		}
	}

	/** @return string */
	protected function getShellParam()
	{
		$firstRow = reset($this->params['ACCUMULATOR']);
		$lastRow = end($this->params['ACCUMULATOR']);

		$param = "";
		if (array_key_exists('ID', $firstRow)) {
			$param = "id from {$firstRow['ID']} to {$lastRow['ID']}";
		}

		if (!$param) {
			$param = randString(6);
		}

		return $param;
	}
}