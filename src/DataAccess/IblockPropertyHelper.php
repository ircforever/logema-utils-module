<?php


namespace Logema\Utils\DataAccess;


use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Type\Collection;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Type\DateTime;

class IblockPropertyHelper
{
	protected $iblockId = -1;

	protected static $iblockPropertiesCache = null;
	protected static $iblockPropertiesEnumCache = null;

	/**
	 * @param int $iblockId
	 */
	protected function __construct(int $iblockId)
	{
		$this->iblockId = $iblockId;
	}

	/**
	 * @param int $iblockId
	 * @return static
	 */
	public static function forIblock(int $iblockId)
	{
		static $instances = [];
		if (!array_key_exists($iblockId, $instances)) {
			$instances[$iblockId] = new static($iblockId);
		}

		return $instances[$iblockId];
	}

	public function getPropertyById(int $id)
	{
		static::initPropertiesCacheIfNeed($this->iblockId);

		return self::$iblockPropertiesCache[$this->iblockId]['PROPERTIES'][$id];
	}

	public function getPropertyByCode(string $code)
	{
		static::initPropertiesCacheIfNeed($this->iblockId);

		$id = self::$iblockPropertiesCache[$this->iblockId]['CODE2ID'][$code];

		return self::$iblockPropertiesCache[$this->iblockId]['PROPERTIES'][$id];
	}

	public function getPropertyByXmlId(string $xmlId)
	{
		static::initPropertiesCacheIfNeed($this->iblockId);

		$id = self::$iblockPropertiesCache[$this->iblockId]['XMLID2ID'][$xmlId];

		return self::$iblockPropertiesCache[$this->iblockId]['PROPERTIES'][$id];
	}

	public function getPropertyEnumById(array $property, int $id)
	{
		static::initPropertiesEnumCacheIfNeed($this->iblockId, $property);

		return self::$iblockPropertiesEnumCache[$this->iblockId][(int)$property['ID']]['ENUMS'][$id];
	}

	public function getPropertyEnum(array $property)
	{
		static::initPropertiesEnumCacheIfNeed($this->iblockId, $property);

		return self::$iblockPropertiesEnumCache[$this->iblockId][(int)$property['ID']]['ENUMS'];
	}

	public function getPropertyEnumByXmlId(array $property, string $xmlId)
	{
		static::initPropertiesEnumCacheIfNeed($this->iblockId, $property);

		$id = self::$iblockPropertiesEnumCache[$this->iblockId][(int)$property['ID']]['XMLID2ID'][$xmlId];

		return self::$iblockPropertiesEnumCache[$this->iblockId][(int)$property['ID']]['ENUMS'][$id];
	}

	public static function clearCache(int $iblockId = 0)
	{
		if ($iblockId) {
			unset(static::$iblockPropertiesEnumCache[$iblockId]);
		} else {
			static::$iblockPropertiesEnumCache = null;
		}
	}

	protected static function initPropertiesCacheIfNeed(int $iblockId)
	{
		if (!array_key_exists($iblockId, static::$iblockPropertiesCache)) {
			static::$iblockPropertiesCache[$iblockId] = [
				'PROPERTIES' => [], //Свойства
				'CODE2ID' => [], //Обратный индекс
				'XMLID2ID' => [] //Обратный индекс
			];
		}

		$iterator = PropertyTable::query()
			->setSelect(['*'])
			->setFilter(['=IBLOCK_ID' => $iblockId])
			->setOrder(['SORT' => 'asc'])
			->exec();

		while ($property = $iterator->fetch()) {
			static::$iblockPropertiesCache[$iblockId]['PROPERTIES'][$property['ID']] = $property;
		}

		//reverse index
		static::$iblockPropertiesCache[$iblockId]['CODE2ID'] = [];
		foreach (static::$iblockPropertiesCache[$iblockId]['PROPERTIES'] as $id => &$property) {
			static::$iblockPropertiesCache[$iblockId]['CODE2ID'][$property['CODE']] = $id;
			static::$iblockPropertiesCache[$iblockId]['XMLID2ID'][$property['XML_ID']] = $id;
		}
		unset($property);
	}

	protected static function initPropertiesEnumCacheIfNeed(int $iblockId, array $property)
	{
		$propertyId = (int)$property['ID'];
		if (!$propertyId) {
			throw new \InvalidArgumentException();
		}

		if (!array_key_exists($iblockId, static::$iblockPropertiesEnumCache)) {
			static::$iblockPropertiesEnumCache[$iblockId][$propertyId] = [
				'ENUMS' => [], //Варианты списка
				'XMLID2ID' => [] //Обратный индекс
			];
		}

		$iterator = PropertyEnumerationTable::query()
			->setSelect(['*'])
			->setFilter([
				'=PROPERTY.IBLOCK.ID' => $iblockId,
				'=PROPERTY_ID' => $propertyId
			])
			->setOrder(['SORT' => 'asc'])
			->exec();

		while ($enum = $iterator->fetch()) {
			static::$iblockPropertiesEnumCache[$iblockId][$propertyId]['ENUMS'][$enum['ID']] = $enum;
		}

		//reverse index
		foreach (static::$iblockPropertiesEnumCache[$iblockId][$propertyId]['ENUMS'] as $id => &$enum) {
			static::$iblockPropertiesEnumCache[$iblockId][$propertyId]['XMLID2ID'][$enum['XML_ID']] = $id;
		}
		unset($enum);
	}

	public function getOneCBooleanPropertyEnumIds($property)
	{
		$true = $this->getPropertyEnumByXmlId($property, 'true');
		$false = $this->getPropertyEnumByXmlId($property, 'false');

		return [$true['ID'], $false['ID']];
	}

	/**
	 * @param array $element
	 * @param string $code
	 * @return int|int[]
	 */
	public function getPropertyValueEnum(&$element, $code)
	{
		$value = $element['PROPERTIES'][$code]['VALUE_ENUM_ID'];

		if (is_array($value)) {
			Collection::normalizeArrayValuesByInt($value);
		} else {
			$value = (int)$value;
		}

		return $value;
	}

	public function getIblockHelper()
	{
		return IblockHelper::forIblock($this->iblockId);
	}
}