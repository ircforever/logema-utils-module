<?php

namespace Logema\Utils\Multithreading;


class WorkerRecord
{
	/** @var int */
	public $id = 0;
	/** @var string */
	public $receivedData = '';
	/** @var Worker */
	public $worker = null;

	/**
	 * WorkerRecord constructor.
	 * @param Worker $worker
	 */
	public function __construct(Worker $worker)
	{
		static $id = -1;
		$id++;

		$this->id = $id;
		$this->worker = $worker;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getReceivedData(): string
	{
		return $this->receivedData;
	}

	/**
	 * @param string $receivedData
	 */
	public function setReceivedData(string $receivedData)
	{
		$this->receivedData = $receivedData;
	}

	public function addReceivedData(string $part)
	{
		$this->receivedData .= $part;
	}

	/**
	 * @return Worker
	 */
	public function getWorker(): Worker
	{
		return $this->worker;
	}

	/**
	 * @param Worker $worker
	 */
	public function setWorker(Worker $worker)
	{
		$this->worker = $worker;
	}
}
