#### Пример реализации скрипта, запускающего задачу chunkTask

- Установка констант для отключения агентов и статистики
  ```
  define("NO_KEEP_STATISTIC", true);
  define('CHK_EVENT', false);
  define('NO_AGENT_CHECK', true);
  define("STATISTIC_SKIP_ACTIVITY_CHECK", true);
  define('BX_SECURITY_SESSION_VIRTUAL', true);
  define('SITE_ID', 's1');
  ```
- Подключение пролога

  Внутри Bitrix Framework активно используется `$_SERVER['DOCUMENT_ROOT']`.
  Соответственно, нужно задать её вручную, т.к. происходит не обычный хит к сайту
  и в `$_SERVER['DOCUMENT_ROOT']` лежит директория скрипта.

  ```
  $_SERVER['DOCUMENT_ROOT'] = dirname(__DIR__, 3);
  require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
  ```

- Сброс буферизации и настройка вывода ошибок
  ```
  while (ob_get_level()) {
      ob_end_clean();
  }
  
  error_reporting(0);
  ```

- Защита от одновременного запуска нескольких мастер - процессов

  Если в аргументах php лежит только путь к скрипту, создается мастер - процесс.
  Во время работы одного мастер - процесса должны запускаться только рабочие процессы.

  ```
  if (count($argv) == 1) {
      $me = fopen(__FILE__, 'r');
      $locked = flock($me, LOCK_EX | LOCK_NB);
      if (!$locked) {
          die();
      }
  }
  ```

- Создание и запуск задачи

  Создается объект задачи и соответствующий объект обработчика задач.
  Аргументы обработчика - число запускаемых рабочих процессов и аргументы php.
  `$result` - объект `\Bitrix\Main\Result`, возвращенный методом `finalize`.

  ```
  $task = new NullChunkTask();
  $task->setParams(['testParam' => 'test']);
  $processor = new PoolChunkableTaskProcessor(4, $argv);
  $result = $processor->process($task);
  ```

- Обработка результата

  ```
  if (!$result->isSuccess()) {
      echo "Ошибка:" . implode("\n", $result->getErrorMessages());

      exit(1);
  }
  ```

##### Код полностью
```
<?php

use Logema\Utils\Multithreading\Example\NullChunkTask;
use Logema\Utils\Multithreading\PoolChunkableTaskProcessor;

define("NO_KEEP_STATISTIC", true);
define('CHK_EVENT', false);
define('NO_AGENT_CHECK', true);
define("STATISTIC_SKIP_ACTIVITY_CHECK", true);
define('BX_SECURITY_SESSION_VIRTUAL', true);
define('SITE_ID', 's1');

$_SERVER['DOCUMENT_ROOT'] = dirname(__DIR__, 3);
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

while (ob_get_level()) {
    ob_end_clean();
}

error_reporting(0);

//Если нужно защититься от параллельного запуска нескольких копий
if (count($argv) == 1) {
    $me = fopen(__FILE__, 'r');
    $locked = flock($me, LOCK_EX | LOCK_NB);
    if (!$locked) {
        die();
    }
}

$task = new NullChunkTask();
$task->setParams(['testParam' => 'test']);//Пример задания параметров задачи
$processor = new PoolChunkableTaskProcessor(4, $argv); //Мы будем обрабатывать задачу в 4 потока
$result = $processor->process($task);

if (!$result->isSuccess()) {
    echo "Ошибка:" . implode("\n", $result->getErrorMessages());

    exit(1);
}
```