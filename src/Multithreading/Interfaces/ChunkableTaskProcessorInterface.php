<?php

namespace Logema\Utils\Multithreading\Interfaces;


/**
 * Штука, которая делает какую-то работу по частям и возвращает результат
 */
interface ChunkableTaskProcessorInterface
{
	/**
	 * @param ChunkableTaskInterface $task
	 * @return \Bitrix\Main\Result
	 */
	public function process(ChunkableTaskInterface $task);
}